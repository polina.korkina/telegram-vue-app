export default [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/HomeView.vue'),
  },
  {
    path: '/second-page',
    name: 'second-page',
    component: () => import('@/views/SecondPageView.vue'),
  },
];
