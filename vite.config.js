import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import * as path from 'path';
import mkcert from 'vite-plugin-mkcert';

// https://vitejs.dev/config/

export default defineConfig({
  base: './',
  plugins: [vue(), mkcert()],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  server: {
    host: '0.0.0.0',
    hmr: {
      host: 'tg.local',
    },
  },
});
